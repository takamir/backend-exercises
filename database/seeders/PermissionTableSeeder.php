<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;

class PermissionTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //

        //Product permission list
        Permission::create(['name' => 'products.index']);
        Permission::create(['name' => 'products.edit']);
        Permission::create(['name' => 'products.show']);
        Permission::create(['name' => 'products.create']);
        Permission::create(['name' => 'products.search']);
        Permission::create(['name' => 'products.destroy']);

        //Sales permission list

        Permission::create(['name' => 'sales.index']);
        Permission::create(['name' => 'sales.edit']);
        Permission::create(['name' => 'sales.show']);
        Permission::create(['name' => 'sales.create']);
        Permission::create(['name' => 'sales.destroy']);

        //User permission list
        Permission::create(['name' => 'users.index']);
        Permission::create(['name' => 'users.edit']);
        Permission::create(['name' => 'users.show']);
        Permission::create(['name' => 'users.create']);
        Permission::create(['name' => 'users.destroy']);

        //Admin
        $admin = Role::create(['name' => 'admin']);

        $admin->givePermissionTo([
            'users.index',
            'users.edit',
            'users.show',
            'users.create',
            'users.destroy',
        ]);
        //$admin->givePermissionTo('products.index');
        //$admin->givePermissionTo(Permission::all());

        //Operator
        $operator = Role::create(['name' => 'operator']);

        $operator->givePermissionTo([
            'products.index',
            'products.edit',
            'products.show',
            'products.create',
            'products.search',
            'products.destroy'
        ]);
        //Sales
        $sales = Role::create(['name' => 'sales']);

        $sales->givePermissionTo([
            'sales.index',
            'sales.edit',
            'sales.show',
            'sales.create',
            'sales.destroy'
        ]);

    }
}
