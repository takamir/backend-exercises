<?php

namespace Database\Factories;

use App\Models\Sale;
use Illuminate\Database\Eloquent\Factories\Factory;

class SaleFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Sale::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $random_quantity = $this->faker->randomDigit();
        $random_price =  $this->faker->randomFloat();
        return [
            'SKU' => $this->faker->unique()->text(10) ,
            'quantity' => $random_quantity,
            'price' => $random_price,
            'total_price' => $random_quantity*$random_price
        ];
    }
}
