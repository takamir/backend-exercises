<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Models\Product;
use App\Models\Sale;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class SaleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function sellProduct(Request $request)
    {
        //Validate request
        $validator = Validator::make($request->all(), [
            'sku' => 'required|string',
            'quantity' => 'required|numeric|min:1',
            'price'  => 'required|numeric|min:0.01'
        ]);

        if($validator->fails()){
            return response()->json($validator->errors()->toJson(), 400);

        }


        //Check if product id exist
        if (Product::where('sku', $request->get('sku'))->exists()) {

            if(Product::where('quantity','>','0')->where('sku', $request->get('sku'))->where('status','=','1')->where('quantity','>=',$request->get('quantity'))->exists()){
                $product = Product::where('sku', $request->get('sku'))->first();
                $product->quantity = $product->quantity - $request->get('quantity');
                $sale = new Sale();
                $sale->sku = $request->get('sku');
                $sale->quantity = $request->get('quantity');
                $sale->price = $request->get('price');
                $sale->total_price = ($request->get('quantity') * $request->get('price'));

                $product->save();
                $sale->save();
                return response()->json([
                    "message" => "records updated successfully"
                ], 200);


            }else{
                //Product quantity on inventary is lower than expected to sell
                return response()->json([
                    "message" => "Product quantity on inventary is lower than expected to sell"
                ], 500);
            }
        }  else{
            //Product sku doesn't exist or not found
            return response()->json([
                "message" => "SKU not found"
            ], 404);

        }

    }



}
