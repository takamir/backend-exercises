<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Sale extends Model
{
    use HasFactory;
    protected $guard_name = 'api';

    protected $fillable = [
        'SKU',
        'quantity',
        'price',
        'total_price',
    ];
}
