
# Laravel TEST
  
Backend project made with Laravel 8 using API features.  
  

## Ejercicio

Un cliente requiere un pequeño control de inventario de los productos de su almacén.

Actualmente la aplicación la usaran solo una persona como administrador, dos operadores y un cajero.

**Módulos/Casos**

**1.** **Usuarios**: Módulo de administración de usuarios para el cual se consideran los siguientes datos: nombre, apellido, email, estado(activo, inactivo), rol(administrador, operador, cajero), contraseña(encriptada)

a.Ingreso de usuarios

i.El empleado con rol _administrador_ puede crear usuarios

b.Listado de usuarios

ii. El empleado con rol _administrador_ listar todos los usuarios

c.Actualizacion de usuarios

iii.El empleado con rol _administrador_ puede editar los usuarios

d.Eliminacion de usuarios

iv. El empleado con rol _administrador_ puede cambiar el estado de los usuarios. Para la acción de eliminar cambiar el estado a inactivo.

**2.** **Autenticación:** Módulo de autenticación el cual validar el ingreso de los usuarios al sistema

e. Login

v. El empleado debe ingresar a la aplicación mediante una pantalla de acceso, haciendo uso de usuario y contraseña. (Considerar solo usuarios activos)

**3.** **Productos:** Módulo de administración de productos, para el cual se consideran los siguientes datos: nombre, sku, precio, disponibles, bodega, área, estado(activo, inactivo)

f.Esta opción solo estará disponible para el rol _operador._

g.Ingreso de producto

vi.El empleado _operador_ elige la opción “**Productos**” del menú principal, debe seleccionar “Agregar” e ingresar los datos para almacenar el producto

h.Listado de productos

vii.El empleado _operador_ elige la opción “Productos” del menú principal debe listar los productos almacenados

viii.Esta vista deberá tener filtros para poder buscar por SKU.

ix. Se requiere principalmente poder visualizar la cantidad de productos disponibles, la bodega y el área donde se encuentran

i.Actualización de productos

x.El empleado _operador_ elige la opción “Productos” del menú principal, debe seleccionar el producto el cual desea editar

xi. Por lo general esta opción se usará para actualizar la cantidad de disponibles del producto

j. Eliminación de productos

xii. Para esta tarea se requiere cambiar el estado del producto

**4.** **Ventas**: Modulo de ventas, para este módulo se considera que se guarden los siguientes registros: SKU, cantidad, precio, fecha_venta, total_venta(cantidad*precio)

k.Esta opción solo estará disponible para el rol de cajero_._

l.Ventas

xiii. El usuario _cajero_ deberá ingresar a la opción “Ventas” e ingresar el SKU del producto, ingresar la cantidad que desea vender y hacer la operación para actualizar la cantidad disponible del producto
  
# How to test endpoints  
1. Use POSTMAN documentation link:  https://documenter.getpostman.com/view/531434/TVzSkHJo  
2. Use login to get and set JWT token before using other endpoints  
  
# How to install project locally  
1. Clone repository from GIT: https://takamir@bitbucket.org/takamir/backend-exercises.git
2. Generate a new .env file with your setting.  
3. Use php artisan jwt:secret to generate a new key.  
4. You can run migrations and seeders or import the MySQL Dump file "now_test_db.sql" from root directory.  
5. Use PHP artisan serve  
6. Connect POSTMAN to your local URL (Postman collection file is in root directory "API.postman_collection.json")
