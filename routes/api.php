<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\API\UserController;
use App\Http\Controllers\API\ProductController;
use App\Http\Controllers\API\JWTAuthController;
use \App\Http\Controllers\API\SaleController;
use App\Http\Controllers\API\PasswordResetRequestController;
use App\Http\Controllers\API\ChangePasswordController;
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/


Route::post('login', [UserController::class, 'login']);



Route::group(['middleware' => ['role:admin','jwt.auth']], function () {
    Route::post('register', [UserController::class, 'store']);
    Route::apiResource('user',UserController::class);
    Route::post('getUser',[UserController::class, 'getUser']);
});


Route::group(['middleware' => ['role:operator','jwt.auth']], function () {
    //Product API routes
    Route::apiResource('products',ProductController::class);
    Route::post('products/{id}',[ProductController::class,'update']);
    Route::post('products-search',[ProductController::class,'search']);
});

Route::group(['middleware' => ['role:sales','jwt.auth']], function () {
    //Sales API routes
    Route::post('sales/sell',[SaleController::class,'sellProduct']);
});

