/*
 Navicat Premium Data Transfer

 Source Server         : Localhost_WAMPP
 Source Server Type    : MySQL
 Source Server Version : 50731
 Source Host           : localhost:3306
 Source Schema         : now_test_db

 Target Server Type    : MySQL
 Target Server Version : 50731
 File Encoding         : 65001

 Date: 12/01/2021 22:26:12
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for failed_jobs
-- ----------------------------
DROP TABLE IF EXISTS `failed_jobs`;
CREATE TABLE `failed_jobs`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `uuid` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `connection` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0),
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `failed_jobs_uuid_unique`(`uuid`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of failed_jobs
-- ----------------------------

-- ----------------------------
-- Table structure for migrations
-- ----------------------------
DROP TABLE IF EXISTS `migrations`;
CREATE TABLE `migrations`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 7 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of migrations
-- ----------------------------
INSERT INTO `migrations` VALUES (1, '2014_10_12_000000_create_users_table', 1);
INSERT INTO `migrations` VALUES (2, '2014_10_12_100000_create_password_resets_table', 1);
INSERT INTO `migrations` VALUES (3, '2019_08_19_000000_create_failed_jobs_table', 1);
INSERT INTO `migrations` VALUES (4, '2021_01_07_015543_create_products_table', 1);
INSERT INTO `migrations` VALUES (5, '2021_01_12_192529_create_permission_tables', 1);
INSERT INTO `migrations` VALUES (6, '2021_01_12_195921_create_sales_table', 1);

-- ----------------------------
-- Table structure for model_has_permissions
-- ----------------------------
DROP TABLE IF EXISTS `model_has_permissions`;
CREATE TABLE `model_has_permissions`  (
  `permission_id` bigint(20) UNSIGNED NOT NULL,
  `model_type` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `model_id` bigint(20) UNSIGNED NOT NULL,
  PRIMARY KEY (`permission_id`, `model_id`, `model_type`) USING BTREE,
  INDEX `model_has_permissions_model_id_model_type_index`(`model_id`, `model_type`) USING BTREE,
  CONSTRAINT `model_has_permissions_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of model_has_permissions
-- ----------------------------

-- ----------------------------
-- Table structure for model_has_roles
-- ----------------------------
DROP TABLE IF EXISTS `model_has_roles`;
CREATE TABLE `model_has_roles`  (
  `role_id` bigint(20) UNSIGNED NOT NULL,
  `model_type` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `model_id` bigint(20) UNSIGNED NOT NULL,
  PRIMARY KEY (`role_id`, `model_id`, `model_type`) USING BTREE,
  INDEX `model_has_roles_model_id_model_type_index`(`model_id`, `model_type`) USING BTREE,
  CONSTRAINT `model_has_roles_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of model_has_roles
-- ----------------------------
INSERT INTO `model_has_roles` VALUES (1, 'App\\Models\\User', 6);
INSERT INTO `model_has_roles` VALUES (3, 'App\\Models\\User', 7);
INSERT INTO `model_has_roles` VALUES (2, 'App\\Models\\User', 8);
INSERT INTO `model_has_roles` VALUES (1, 'App\\Models\\User', 9);

-- ----------------------------
-- Table structure for password_resets
-- ----------------------------
DROP TABLE IF EXISTS `password_resets`;
CREATE TABLE `password_resets`  (
  `email` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  INDEX `password_resets_email_index`(`email`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of password_resets
-- ----------------------------

-- ----------------------------
-- Table structure for permissions
-- ----------------------------
DROP TABLE IF EXISTS `permissions`;
CREATE TABLE `permissions`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `guard_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 17 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of permissions
-- ----------------------------
INSERT INTO `permissions` VALUES (1, 'products.index', 'api', '2021-01-13 01:23:24', '2021-01-13 01:23:24');
INSERT INTO `permissions` VALUES (2, 'products.edit', 'api', '2021-01-13 01:23:24', '2021-01-13 01:23:24');
INSERT INTO `permissions` VALUES (3, 'products.show', 'api', '2021-01-13 01:23:24', '2021-01-13 01:23:24');
INSERT INTO `permissions` VALUES (4, 'products.create', 'api', '2021-01-13 01:23:24', '2021-01-13 01:23:24');
INSERT INTO `permissions` VALUES (5, 'products.search', 'api', '2021-01-13 01:23:24', '2021-01-13 01:23:24');
INSERT INTO `permissions` VALUES (6, 'products.destroy', 'api', '2021-01-13 01:23:24', '2021-01-13 01:23:24');
INSERT INTO `permissions` VALUES (7, 'sales.index', 'api', '2021-01-13 01:23:24', '2021-01-13 01:23:24');
INSERT INTO `permissions` VALUES (8, 'sales.edit', 'api', '2021-01-13 01:23:24', '2021-01-13 01:23:24');
INSERT INTO `permissions` VALUES (9, 'sales.show', 'api', '2021-01-13 01:23:24', '2021-01-13 01:23:24');
INSERT INTO `permissions` VALUES (10, 'sales.create', 'api', '2021-01-13 01:23:24', '2021-01-13 01:23:24');
INSERT INTO `permissions` VALUES (11, 'sales.destroy', 'api', '2021-01-13 01:23:24', '2021-01-13 01:23:24');
INSERT INTO `permissions` VALUES (12, 'users.index', 'api', '2021-01-13 01:23:24', '2021-01-13 01:23:24');
INSERT INTO `permissions` VALUES (13, 'users.edit', 'api', '2021-01-13 01:23:24', '2021-01-13 01:23:24');
INSERT INTO `permissions` VALUES (14, 'users.show', 'api', '2021-01-13 01:23:24', '2021-01-13 01:23:24');
INSERT INTO `permissions` VALUES (15, 'users.create', 'api', '2021-01-13 01:23:24', '2021-01-13 01:23:24');
INSERT INTO `permissions` VALUES (16, 'users.destroy', 'api', '2021-01-13 01:23:24', '2021-01-13 01:23:24');

-- ----------------------------
-- Table structure for products
-- ----------------------------
DROP TABLE IF EXISTS `products`;
CREATE TABLE `products`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `SKU` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `warehouse` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `area` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `quantity` int(11) NOT NULL,
  `price` decimal(13, 2) NULL DEFAULT NULL,
  `status` int(11) NOT NULL,
  `description` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `image_url` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `products_sku_unique`(`SKU`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 52 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of products
-- ----------------------------
INSERT INTO `products` VALUES (1, 'Oberbrunner Inc', 'Et libero.', 'San Miguel', 'Casa', 7, 1085.77, 0, 'Minima voluptatem voluptas eaque.', NULL, '2021-01-12 20:06:31', '2021-01-12 20:06:31');
INSERT INTO `products` VALUES (2, 'Herman-Hansen', 'Quo minus.', 'Lourdes', 'Animales', 8, 2622.15, 0, 'Ut veniam a temporibus in quisquam.', NULL, '2021-01-12 20:06:31', '2021-01-12 20:06:31');
INSERT INTO `products` VALUES (3, 'Fay LLC', 'Qui.', 'El Salvador', 'Animales', 0, 23.23, 1, 'Animi eos et recusandae accusamus.', NULL, '2021-01-12 20:06:31', '2021-01-13 03:04:36');
INSERT INTO `products` VALUES (4, 'Cremin, Donnelly and Rodriguez', 'Repellat.', 'El Salvador', 'Tecnología', 1, 287303.30, 0, 'Ex quo nam ipsam quaerat quidem molestias.', NULL, '2021-01-12 20:06:31', '2021-01-12 20:06:31');
INSERT INTO `products` VALUES (5, 'Glover PLC', 'Amet.', 'San Miguel', 'Tecnología', 5, 3520.36, 1, 'Architecto nam recusandae vitae.', NULL, '2021-01-12 20:06:31', '2021-01-13 02:56:43');
INSERT INTO `products` VALUES (6, 'Swaniawski-Kozey', 'Maiores.', 'San Miguel', 'Casa', 0, 4228.95, 0, 'Accusamus ut sint neque voluptatem aspernatur aut.', NULL, '2021-01-12 20:06:31', '2021-01-12 20:06:31');
INSERT INTO `products` VALUES (7, 'Sipes LLC', 'Expedita.', 'El Salvador', 'Casa', 5, 18.34, 0, 'In aliquam suscipit ut molestias rerum.', NULL, '2021-01-12 20:06:32', '2021-01-12 20:06:32');
INSERT INTO `products` VALUES (8, 'Gislason, Gutmann and Moen', 'Earum.', 'El Salvador', 'Animales', 2, 1.00, 0, 'Deleniti velit quasi sit ea ducimus similique.', NULL, '2021-01-12 20:06:32', '2021-01-12 20:06:32');
INSERT INTO `products` VALUES (9, 'Gutmann Inc', 'Aut autem.', 'Lourdes', 'Animales', 7, 1646665.50, 0, 'Aliquam cum voluptatum molestiae quo.', NULL, '2021-01-12 20:06:32', '2021-01-12 20:06:32');
INSERT INTO `products` VALUES (10, 'Jenkins and Sons', 'Non iure.', 'Opico', 'Tecnología', 4, 5.71, 1, 'Quis et provident ab.', NULL, '2021-01-12 20:06:32', '2021-01-12 20:06:32');
INSERT INTO `products` VALUES (11, 'Gleichner-Green', 'Eligendi.', 'San Miguel', 'Animales', 8, 1247121.13, 1, 'Accusamus numquam sapiente ipsa sunt repudiandae a molestiae.', NULL, '2021-01-12 20:06:32', '2021-01-12 20:06:32');
INSERT INTO `products` VALUES (12, 'DuBuque, Lubowitz and Little', 'Et sint.', 'El Salvador', 'Animales', 5, 4074.56, 1, 'Ab consequatur exercitationem cumque harum consequatur.', NULL, '2021-01-12 20:06:32', '2021-01-12 20:06:32');
INSERT INTO `products` VALUES (13, 'Casper-Jaskolski', 'Dolorum.', 'San Miguel', 'Casa', 8, 45.28, 0, 'Commodi quaerat cumque quidem harum vel ad.', NULL, '2021-01-12 20:06:32', '2021-01-12 20:06:32');
INSERT INTO `products` VALUES (14, 'Gottlieb LLC', 'Sed odit.', 'San Miguel', 'Tecnología', 7, 55547.50, 1, 'Enim quos voluptate ab tenetur ut quo.', NULL, '2021-01-12 20:06:32', '2021-01-12 20:06:32');
INSERT INTO `products` VALUES (15, 'O\'Keefe, Wehner and Purdy', 'Rem.', 'San Miguel', 'Tecnología', 9, 7.00, 1, 'Blanditiis laborum officia facilis.', NULL, '2021-01-12 20:06:32', '2021-01-12 20:06:32');
INSERT INTO `products` VALUES (16, 'Harber, Bosco and Jaskolski', 'Qui fugit.', 'San Miguel', 'Animales', 2, 536742.85, 1, 'Quia aut rerum id non.', NULL, '2021-01-12 20:06:32', '2021-01-12 20:06:32');
INSERT INTO `products` VALUES (17, 'Pfannerstill-Ryan', 'Sunt eum.', 'El Salvador', 'Casa', 0, 3.45, 0, 'Nihil explicabo et expedita laboriosam.', NULL, '2021-01-12 20:06:32', '2021-01-12 20:06:32');
INSERT INTO `products` VALUES (18, 'Breitenberg PLC', 'Dolor.', 'El Salvador', 'Casa', 1, 1802.82, 0, 'Vitae reprehenderit eius unde cum quisquam sed recusandae.', NULL, '2021-01-12 20:06:32', '2021-01-12 20:06:32');
INSERT INTO `products` VALUES (19, 'Lesch, Reinger and Murphy', 'Similique.', 'El Salvador', 'Tecnología', 7, 724649.02, 1, 'Eius aut iure ut sit.', NULL, '2021-01-12 20:06:32', '2021-01-12 20:06:32');
INSERT INTO `products` VALUES (20, 'Howe-Wolff', 'Unde.', 'Lourdes', 'Casa', 9, 0.00, 0, 'Eligendi doloribus at maxime et.', NULL, '2021-01-12 20:06:32', '2021-01-12 20:06:32');
INSERT INTO `products` VALUES (21, 'Volkman Inc', 'Ducimus.', 'Opico', 'Tecnología', 0, 21828.00, 0, 'Repellendus non unde ut quia.', NULL, '2021-01-12 20:06:32', '2021-01-12 20:06:32');
INSERT INTO `products` VALUES (22, 'D\'Amore-Heathcote', 'Eum.', 'El Salvador', 'Animales', 6, 23264107.63, 0, 'Nemo qui eum molestias eaque.', NULL, '2021-01-12 20:06:32', '2021-01-12 20:06:32');
INSERT INTO `products` VALUES (23, 'Schmeler Group', 'Iure.', 'El Salvador', 'Animales', 6, 513.58, 0, 'Totam ducimus aut culpa ullam eius harum incidunt.', NULL, '2021-01-12 20:06:32', '2021-01-12 20:06:32');
INSERT INTO `products` VALUES (24, 'Hintz-Gutkowski', 'Rerum quo.', 'San Miguel', 'Tecnología', 5, 232.12, 1, 'Qui voluptas nemo dolor unde perspiciatis.', NULL, '2021-01-12 20:06:32', '2021-01-12 20:06:32');
INSERT INTO `products` VALUES (25, 'Greenholt, Wilkinson and Dickinson', 'Ut rem.', 'Opico', 'Casa', 0, 26172751.64, 0, 'Ullam eveniet blanditiis vero mollitia recusandae iste ut.', NULL, '2021-01-12 20:06:32', '2021-01-12 20:06:32');
INSERT INTO `products` VALUES (26, 'Stehr PLC', 'Fuga qui.', 'Lourdes', 'Casa', 3, 10851024.20, 0, 'Fugit eos fugiat illum.', NULL, '2021-01-12 20:06:32', '2021-01-12 20:06:32');
INSERT INTO `products` VALUES (27, 'Ebert-Mayert', 'Dicta.', 'Lourdes', 'Animales', 4, 34394744.34, 1, 'Est rerum rem eum illum.', NULL, '2021-01-12 20:06:32', '2021-01-12 20:06:32');
INSERT INTO `products` VALUES (28, 'Romaguera, Kiehn and Bergstrom', 'Sequi.', 'San Miguel', 'Tecnología', 7, 4287.00, 0, 'Est nostrum ut nihil et fugit sed dignissimos neque.', NULL, '2021-01-12 20:06:32', '2021-01-12 20:06:32');
INSERT INTO `products` VALUES (29, 'Durgan and Sons', 'Eius.', 'Lourdes', 'Casa', 6, 963.00, 1, 'Magni iure dolorum velit ea est beatae id cumque.', NULL, '2021-01-12 20:06:32', '2021-01-12 20:06:32');
INSERT INTO `products` VALUES (30, 'Turner-Bins', 'Suscipit.', 'El Salvador', 'Animales', 1, 453438.91, 0, 'Eaque nobis illum consequuntur molestiae odit et facilis atque.', NULL, '2021-01-12 20:06:32', '2021-01-12 20:06:32');
INSERT INTO `products` VALUES (31, 'Carter, Padberg and Haley', 'Sed.', 'Opico', 'Animales', 8, 419820.37, 1, 'Assumenda dolorum et delectus quaerat aliquid quia qui.', NULL, '2021-01-12 20:06:32', '2021-01-12 20:06:32');
INSERT INTO `products` VALUES (32, 'Schroeder Inc', 'Eveniet.', 'El Salvador', 'Tecnología', 7, 742787.10, 0, 'Atque alias est ad eos qui.', NULL, '2021-01-12 20:06:32', '2021-01-12 20:06:32');
INSERT INTO `products` VALUES (33, 'Farrell-Dickens', 'Aperiam.', 'San Miguel', 'Tecnología', 7, 3.30, 1, 'Natus ut minima facere explicabo voluptatem quas optio excepturi.', NULL, '2021-01-12 20:06:32', '2021-01-12 20:06:32');
INSERT INTO `products` VALUES (34, 'Koss Inc', 'Est.', 'Lourdes', 'Casa', 0, 12.78, 1, 'Sint vel illum ea enim aliquam.', NULL, '2021-01-12 20:06:32', '2021-01-12 20:06:32');
INSERT INTO `products` VALUES (35, 'Waters-Grady', 'Aut ea.', 'San Miguel', 'Casa', 2, 2926.02, 0, 'Neque sed doloribus maiores dolor quae.', NULL, '2021-01-12 20:06:32', '2021-01-12 20:06:32');
INSERT INTO `products` VALUES (36, 'Reichert-Corwin', 'Fugiat.', 'Lourdes', 'Animales', 1, 105781.31, 0, 'Sed quas quasi sed nobis.', NULL, '2021-01-12 20:06:32', '2021-01-12 20:06:32');
INSERT INTO `products` VALUES (37, 'Gorczany, Schmeler and Ward', 'Nisi.', 'Lourdes', 'Animales', 5, 229.20, 1, 'Aut ex sapiente aut voluptatem qui qui.', NULL, '2021-01-12 20:06:32', '2021-01-12 20:06:32');
INSERT INTO `products` VALUES (38, 'Strosin, Schroeder and Brakus', 'Odit.', 'San Miguel', 'Animales', 9, 331.88, 1, 'Praesentium fugiat amet pariatur voluptatibus.', NULL, '2021-01-12 20:06:32', '2021-01-12 20:06:32');
INSERT INTO `products` VALUES (39, 'Beahan-Quigley', 'Tenetur.', 'Opico', 'Animales', 0, 157636916.58, 0, 'Quia voluptas officia eos dolores et id.', NULL, '2021-01-12 20:06:32', '2021-01-12 20:06:32');
INSERT INTO `products` VALUES (40, 'Upton LLC', 'Est aut.', 'Opico', 'Casa', 0, 0.16, 0, 'Eligendi occaecati ipsa similique qui velit neque.', NULL, '2021-01-12 20:06:32', '2021-01-12 20:06:32');
INSERT INTO `products` VALUES (41, 'Kautzer-Muller', 'Ut ut eos.', 'Lourdes', 'Animales', 3, 0.86, 0, 'Alias incidunt commodi quo debitis quia fugiat vero.', NULL, '2021-01-12 20:06:32', '2021-01-12 20:06:32');
INSERT INTO `products` VALUES (42, 'King and Sons', 'Neque est.', 'Lourdes', 'Casa', 5, 1.97, 0, 'Libero ut ratione sunt laborum asperiores.', NULL, '2021-01-12 20:06:32', '2021-01-12 20:06:32');
INSERT INTO `products` VALUES (43, 'Cremin Inc', 'Animi.', 'El Salvador', 'Animales', 8, 94998.78, 1, 'Error in blanditiis mollitia accusantium.', NULL, '2021-01-12 20:06:32', '2021-01-12 20:06:32');
INSERT INTO `products` VALUES (44, 'Hammes PLC', 'Illo.', 'El Salvador', 'Tecnología', 1, 4665870.25, 0, 'Blanditiis quia sit expedita quas qui.', NULL, '2021-01-12 20:06:32', '2021-01-12 20:06:32');
INSERT INTO `products` VALUES (45, 'Stark-Leffler', 'Dolores.', 'San Miguel', 'Animales', 9, 87131.65, 1, 'Fugiat dolore qui voluptatem voluptatem nesciunt.', NULL, '2021-01-12 20:06:32', '2021-01-12 20:06:32');
INSERT INTO `products` VALUES (46, 'Lang, Kertzmann and Senger', 'Iste.', 'Opico', 'Tecnología', 3, 77410.49, 1, 'Error sint eos commodi et voluptate quisquam quis.', NULL, '2021-01-12 20:06:32', '2021-01-12 20:06:32');
INSERT INTO `products` VALUES (47, 'Schimmel, Stehr and Mann', 'Quo sed.', 'San Miguel', 'Casa', 7, 147172306.43, 1, 'Animi rem fuga ut cumque.', NULL, '2021-01-12 20:06:32', '2021-01-12 20:06:32');
INSERT INTO `products` VALUES (48, 'Mante PLC', 'A quaerat.', 'El Salvador', 'Tecnología', 0, 145235798.09, 0, 'Vero commodi suscipit eius eos delectus.', NULL, '2021-01-12 20:06:32', '2021-01-12 20:06:32');
INSERT INTO `products` VALUES (49, 'Kohler, Kuhlman and Ondricka', 'Commodi.', 'San Miguel', 'Casa', 2, 552078.77, 0, 'Laboriosam eum temporibus nam aut provident.', NULL, '2021-01-12 20:06:32', '2021-01-12 20:06:32');
INSERT INTO `products` VALUES (50, 'Nader, Cole and Konopelski', 'Placeat.', 'Lourdes', 'Tecnología', 0, 5910.22, 0, 'Dolores molestias harum sunt veniam in mollitia.', NULL, '2021-01-12 20:06:32', '2021-01-12 20:06:32');
INSERT INTO `products` VALUES (51, 'Product 4_modified_2', 'PRD4_modified_2', 'San Miguel', 'Animales', 3, 26.20, 0, 'Product description 1', NULL, '2021-01-13 02:13:02', '2021-01-13 02:24:19');

-- ----------------------------
-- Table structure for role_has_permissions
-- ----------------------------
DROP TABLE IF EXISTS `role_has_permissions`;
CREATE TABLE `role_has_permissions`  (
  `permission_id` bigint(20) UNSIGNED NOT NULL,
  `role_id` bigint(20) UNSIGNED NOT NULL,
  PRIMARY KEY (`permission_id`, `role_id`) USING BTREE,
  INDEX `role_has_permissions_role_id_foreign`(`role_id`) USING BTREE,
  CONSTRAINT `role_has_permissions_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE ON UPDATE RESTRICT,
  CONSTRAINT `role_has_permissions_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of role_has_permissions
-- ----------------------------
INSERT INTO `role_has_permissions` VALUES (12, 1);
INSERT INTO `role_has_permissions` VALUES (13, 1);
INSERT INTO `role_has_permissions` VALUES (14, 1);
INSERT INTO `role_has_permissions` VALUES (15, 1);
INSERT INTO `role_has_permissions` VALUES (16, 1);
INSERT INTO `role_has_permissions` VALUES (1, 2);
INSERT INTO `role_has_permissions` VALUES (2, 2);
INSERT INTO `role_has_permissions` VALUES (3, 2);
INSERT INTO `role_has_permissions` VALUES (4, 2);
INSERT INTO `role_has_permissions` VALUES (5, 2);
INSERT INTO `role_has_permissions` VALUES (6, 2);
INSERT INTO `role_has_permissions` VALUES (7, 3);
INSERT INTO `role_has_permissions` VALUES (8, 3);
INSERT INTO `role_has_permissions` VALUES (9, 3);
INSERT INTO `role_has_permissions` VALUES (10, 3);
INSERT INTO `role_has_permissions` VALUES (11, 3);

-- ----------------------------
-- Table structure for roles
-- ----------------------------
DROP TABLE IF EXISTS `roles`;
CREATE TABLE `roles`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `guard_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 4 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of roles
-- ----------------------------
INSERT INTO `roles` VALUES (1, 'admin', 'api', '2021-01-13 01:23:24', '2021-01-13 01:23:24');
INSERT INTO `roles` VALUES (2, 'operator', 'api', '2021-01-13 01:23:24', '2021-01-13 01:23:24');
INSERT INTO `roles` VALUES (3, 'sales', 'api', '2021-01-13 01:23:24', '2021-01-13 01:23:24');

-- ----------------------------
-- Table structure for sales
-- ----------------------------
DROP TABLE IF EXISTS `sales`;
CREATE TABLE `sales`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `SKU` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `quantity` int(11) NOT NULL,
  `price` decimal(13, 2) NOT NULL,
  `total_price` decimal(13, 2) NOT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 55 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sales
-- ----------------------------
INSERT INTO `sales` VALUES (1, 'Sit eos.', 2, 68945176.36, 137890352.72, '2021-01-12 20:06:43', '2021-01-12 20:06:43');
INSERT INTO `sales` VALUES (2, 'Nulla.', 2, 7.80, 15.60, '2021-01-12 20:06:43', '2021-01-12 20:06:43');
INSERT INTO `sales` VALUES (3, 'Vel.', 1, 4.00, 4.00, '2021-01-12 20:06:43', '2021-01-12 20:06:43');
INSERT INTO `sales` VALUES (4, 'Ea.', 5, 226.76, 1133.79, '2021-01-12 20:06:43', '2021-01-12 20:06:43');
INSERT INTO `sales` VALUES (5, 'Non error.', 6, 0.95, 5.70, '2021-01-12 20:06:43', '2021-01-12 20:06:43');
INSERT INTO `sales` VALUES (6, 'Quia est.', 8, 358.61, 2868.88, '2021-01-12 20:06:43', '2021-01-12 20:06:43');
INSERT INTO `sales` VALUES (7, 'Animi.', 9, 1948.90, 17540.10, '2021-01-12 20:06:43', '2021-01-12 20:06:43');
INSERT INTO `sales` VALUES (8, 'At.', 0, 4835.75, 0.00, '2021-01-12 20:06:43', '2021-01-12 20:06:43');
INSERT INTO `sales` VALUES (9, 'Natus.', 9, 37.20, 334.80, '2021-01-12 20:06:43', '2021-01-12 20:06:43');
INSERT INTO `sales` VALUES (10, 'Quis.', 8, 54.30, 434.40, '2021-01-12 20:06:43', '2021-01-12 20:06:43');
INSERT INTO `sales` VALUES (11, 'Numquam.', 7, 0.02, 0.14, '2021-01-12 20:06:43', '2021-01-12 20:06:43');
INSERT INTO `sales` VALUES (12, 'Tempora.', 0, 198.50, 0.00, '2021-01-12 20:06:43', '2021-01-12 20:06:43');
INSERT INTO `sales` VALUES (13, 'Aut fugit.', 8, 6.91, 55.28, '2021-01-12 20:06:43', '2021-01-12 20:06:43');
INSERT INTO `sales` VALUES (14, 'Ab.', 2, 1.00, 2.00, '2021-01-12 20:06:43', '2021-01-12 20:06:43');
INSERT INTO `sales` VALUES (15, 'Vitae.', 7, 3279.50, 22956.50, '2021-01-12 20:06:43', '2021-01-12 20:06:43');
INSERT INTO `sales` VALUES (16, 'Cumque id.', 6, 52791878.61, 316751271.66, '2021-01-12 20:06:43', '2021-01-12 20:06:43');
INSERT INTO `sales` VALUES (17, 'Pariatur.', 3, 921.89, 2765.68, '2021-01-12 20:06:43', '2021-01-12 20:06:43');
INSERT INTO `sales` VALUES (18, 'Velit.', 9, 10339176.25, 93052586.28, '2021-01-12 20:06:43', '2021-01-12 20:06:43');
INSERT INTO `sales` VALUES (19, 'Ut iste.', 1, 3291.50, 3291.50, '2021-01-12 20:06:43', '2021-01-12 20:06:43');
INSERT INTO `sales` VALUES (20, 'At neque.', 6, 2706.38, 16238.29, '2021-01-12 20:06:43', '2021-01-12 20:06:43');
INSERT INTO `sales` VALUES (21, 'Dolorem.', 0, 4986820.00, 0.00, '2021-01-12 20:06:43', '2021-01-12 20:06:43');
INSERT INTO `sales` VALUES (22, 'Corporis.', 7, 44.81, 313.65, '2021-01-12 20:06:43', '2021-01-12 20:06:43');
INSERT INTO `sales` VALUES (23, 'Dolores.', 2, 1017870.54, 2035741.09, '2021-01-12 20:06:43', '2021-01-12 20:06:43');
INSERT INTO `sales` VALUES (24, 'Minima.', 3, 12257294.94, 36771884.83, '2021-01-12 20:06:43', '2021-01-12 20:06:43');
INSERT INTO `sales` VALUES (25, 'Et.', 0, 95002708.75, 0.00, '2021-01-12 20:06:43', '2021-01-12 20:06:43');
INSERT INTO `sales` VALUES (26, 'Odit.', 4, 6383.06, 25532.25, '2021-01-12 20:06:43', '2021-01-12 20:06:43');
INSERT INTO `sales` VALUES (27, 'Harum.', 9, 0.00, 0.00, '2021-01-12 20:06:43', '2021-01-12 20:06:43');
INSERT INTO `sales` VALUES (28, 'Quo et.', 2, 1054.34, 2108.68, '2021-01-12 20:06:43', '2021-01-12 20:06:43');
INSERT INTO `sales` VALUES (29, 'Similique.', 0, 224.84, 0.00, '2021-01-12 20:06:43', '2021-01-12 20:06:43');
INSERT INTO `sales` VALUES (30, 'Nisi.', 7, 8752.30, 61266.12, '2021-01-12 20:06:43', '2021-01-12 20:06:43');
INSERT INTO `sales` VALUES (31, 'Quasi.', 0, 1466.58, 0.00, '2021-01-12 20:06:43', '2021-01-12 20:06:43');
INSERT INTO `sales` VALUES (32, 'Velit non.', 0, 62556.82, 0.00, '2021-01-12 20:06:43', '2021-01-12 20:06:43');
INSERT INTO `sales` VALUES (33, 'Id dolore.', 8, 1.44, 11.50, '2021-01-12 20:06:43', '2021-01-12 20:06:43');
INSERT INTO `sales` VALUES (34, 'Et aut.', 8, 39.82, 318.59, '2021-01-12 20:06:43', '2021-01-12 20:06:43');
INSERT INTO `sales` VALUES (35, 'Explicabo.', 7, 323.59, 2265.13, '2021-01-12 20:06:43', '2021-01-12 20:06:43');
INSERT INTO `sales` VALUES (36, 'Quidem ut.', 6, 73.88, 443.28, '2021-01-12 20:06:43', '2021-01-12 20:06:43');
INSERT INTO `sales` VALUES (37, 'Quo.', 6, 8845980.04, 53075880.24, '2021-01-12 20:06:43', '2021-01-12 20:06:43');
INSERT INTO `sales` VALUES (38, 'Omnis.', 3, 1.86, 5.57, '2021-01-12 20:06:43', '2021-01-12 20:06:43');
INSERT INTO `sales` VALUES (39, 'Dolore.', 9, 112.21, 1009.93, '2021-01-12 20:06:43', '2021-01-12 20:06:43');
INSERT INTO `sales` VALUES (40, 'Autem id.', 1, 10434.03, 10434.03, '2021-01-12 20:06:43', '2021-01-12 20:06:43');
INSERT INTO `sales` VALUES (41, 'Maxime.', 9, 9069.00, 81621.00, '2021-01-12 20:06:43', '2021-01-12 20:06:43');
INSERT INTO `sales` VALUES (42, 'Mollitia.', 9, 2531.47, 22783.25, '2021-01-12 20:06:43', '2021-01-12 20:06:43');
INSERT INTO `sales` VALUES (43, 'Qui ab.', 7, 453256450.84, 3172795155.86, '2021-01-12 20:06:43', '2021-01-12 20:06:43');
INSERT INTO `sales` VALUES (44, 'Qui nihil.', 6, 6217.67, 37306.02, '2021-01-12 20:06:43', '2021-01-12 20:06:43');
INSERT INTO `sales` VALUES (45, 'Iure.', 1, 369513510.85, 369513510.85, '2021-01-12 20:06:43', '2021-01-12 20:06:43');
INSERT INTO `sales` VALUES (46, 'Fugiat.', 5, 7714450.10, 38572250.50, '2021-01-12 20:06:43', '2021-01-12 20:06:43');
INSERT INTO `sales` VALUES (47, 'Molestias.', 5, 33903.00, 169515.00, '2021-01-12 20:06:43', '2021-01-12 20:06:43');
INSERT INTO `sales` VALUES (48, 'Ab nihil.', 2, 617041879.21, 1234083758.42, '2021-01-12 20:06:43', '2021-01-12 20:06:43');
INSERT INTO `sales` VALUES (49, 'Sequi aut.', 9, 5499.11, 49491.96, '2021-01-12 20:06:43', '2021-01-12 20:06:43');
INSERT INTO `sales` VALUES (50, 'Sed et.', 3, 15.99, 47.98, '2021-01-12 20:06:43', '2021-01-12 20:06:43');
INSERT INTO `sales` VALUES (51, 'Amet.', 3, 3520.36, 10561.08, '2021-01-13 02:56:43', '2021-01-13 02:56:43');
INSERT INTO `sales` VALUES (52, 'Qui.', 3, 3520.36, 10561.08, '2021-01-13 02:57:52', '2021-01-13 02:57:52');
INSERT INTO `sales` VALUES (53, 'Qui.', 3, 3520.36, 10561.08, '2021-01-13 03:00:20', '2021-01-13 03:00:20');
INSERT INTO `sales` VALUES (54, 'Qui.', 3, 3520.36, 10561.08, '2021-01-13 03:04:36', '2021-01-13 03:04:36');

-- ----------------------------
-- Table structure for users
-- ----------------------------
DROP TABLE IF EXISTS `users`;
CREATE TABLE `users`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `username` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `date_of_birth` date NOT NULL,
  `email_verified_at` timestamp(0) NULL DEFAULT NULL,
  `password` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` int(11) NOT NULL,
  `remember_token` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `users_username_unique`(`username`) USING BTREE,
  UNIQUE INDEX `users_email_unique`(`email`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 10 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of users
-- ----------------------------
INSERT INTO `users` VALUES (1, 'Juan perez', 'alfonso.haag', 'una43@example.net', '1-469-951-6518', '2004-05-02', '2021-01-12 20:06:25', '$2y$10$esAVQTwyFzFKZCqg7dorxuax6m6UjF3La1DhteoGPmsLQanmMF25.', 0, 'Hrt0TjZ0b6', '2021-01-12 20:06:25', '2021-01-13 01:57:48');
INSERT INTO `users` VALUES (2, 'Tom Wolff', 'abbott.casandra', 'elinor62@example.com', '+1 (553) 618-0248', '2007-01-08', '2021-01-12 20:06:25', '$2y$10$krjtJWiLHrIDiIcQCjmJM.LruP03OAI1UnPYLp8NQDBwx48k/LQyi', 1, '4d0fyvHBHF', '2021-01-12 20:06:25', '2021-01-12 20:06:25');
INSERT INTO `users` VALUES (3, 'Abel Hayes', 'salvatore.kirlin', 'kulas.adriana@example.net', '+1-845-938-2618', '2020-04-15', '2021-01-12 20:06:25', '$2y$10$9WEngWWBT8dLbMXDspNLwe52ZH9/E0LSG7oXX/8sSVFeO2OVI.Riq', 1, 'V6HMwjlg5T', '2021-01-12 20:06:25', '2021-01-12 20:06:25');
INSERT INTO `users` VALUES (4, 'Hilma Graham IV', 'domenic69', 'gibson.horace@example.com', '625-557-5738', '2017-11-05', '2021-01-12 20:06:25', '$2y$10$KrumN8a/Z1UMBUE7ObkhzO7UJQXZqKKKo/DTQETPCUNfUP7JzuHpK', 0, '16AcCCSqAb', '2021-01-12 20:06:25', '2021-01-12 20:06:25');
INSERT INTO `users` VALUES (5, 'Dr. Carey Toy', 'dominique.marquardt', 'halvorson.raphaelle@example.org', '645.279.2603', '1993-06-19', '2021-01-12 20:06:25', '$2y$10$d6hUqEclmkUFRE5e/bI3rurDg2KliGWGfrQEB3q8A/GEtBNf8Psj.', 1, 'ST2vMD41VZ', '2021-01-12 20:06:25', '2021-01-12 20:06:25');
INSERT INTO `users` VALUES (6, 'Takami modified 2', 'takami', 'takami.rodriguez@gmail.com', '61569552', '1993-09-27', NULL, '$2y$10$hAmf35LglUzN0d2NAlWjF.iYYzs8M8Q4SUgtsS4vK8AwIQWZTRHsy', 1, NULL, '2021-01-13 01:26:08', '2021-01-13 01:52:49');
INSERT INTO `users` VALUES (7, 'Salesman', 'sales', 'user@sales.com', '61569552', '1993-09-27', NULL, '$2y$10$TOzQFrCydz7lm4Lzt2QoN..im6IWIT1qW3VClLTS65nYfV4hCyrDW', 1, NULL, '2021-01-13 03:58:54', '2021-01-13 03:58:54');
INSERT INTO `users` VALUES (8, 'Operator', 'operator', 'user@operator.com', '61569552', '1993-09-27', NULL, '$2y$10$BZV7qkeNxEanhy9foLfZG.wAztRAfomC/L1BOouDzEaLEGRQ1uAni', 1, NULL, '2021-01-13 03:59:27', '2021-01-13 03:59:27');
INSERT INTO `users` VALUES (9, 'admin', 'admin', 'user@admin.com', '61569552', '1993-09-27', NULL, '$2y$10$iipc9znm1uNUCCO/7KfBp.csL3trGkbmXu.RTDvQVxNaDZyCsTpNe', 1, NULL, '2021-01-13 04:00:02', '2021-01-13 04:00:02');

SET FOREIGN_KEY_CHECKS = 1;
